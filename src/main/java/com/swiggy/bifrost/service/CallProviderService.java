package com.swiggy.bifrost.service;

import com.swiggy.bifrost.pojo.callprovider.DeliveryExecutiveNumberResponse;
import com.swiggy.bifrost.pojo.callprovider.ProviderCallBackPojo;

public interface CallProviderService {

    DeliveryExecutiveNumberResponse updateDirectCallStatus(ProviderCallBackPojo providerCallBackPojo);

    DeliveryExecutiveNumberResponse getSecondLegNumberResponse(String firstLegNumebr, String virtualNumber, String callUUId);
}
