package com.swiggy.bifrost.service;

import com.swiggy.bifrost.pojo.callprovider.DeliveryExecutiveNumberResponse;
import com.swiggy.bifrost.pojo.callprovider.ProviderCallBackPojo;
import retrofit2.Call;
import retrofit2.http.*;

public interface CallProviderAPI {

    @GET("direct_call/leg2_number")
    Call<DeliveryExecutiveNumberResponse> getLeg2NumberResponse(@Query("leg1_number") String leg1Number, @Query("virtual_number") String virtualNumber,
                                                                @Query("call_uuid") String callUuid);

    @POST("direct_call/status")
    Call<DeliveryExecutiveNumberResponse> updateDirectCallStatus(@Body ProviderCallBackPojo providerCallBackPojo);
}
