package com.swiggy.bifrost.service.impl;

import com.swiggy.bifrost.pojo.callprovider.DeliveryExecutiveNumberResponse;
import com.swiggy.bifrost.pojo.callprovider.ProviderCallBackPojo;
import com.swiggy.bifrost.service.CallProviderService;
import com.swiggy.bifrost.service.CallProviderAPI;
import lombok.extern.slf4j.Slf4j;
import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class CallProviderServiceImpl implements CallProviderService {
    private CallProviderAPI callProviderAPI;
    @Value("${heimdallUrl}")
    private String heimdallUrl;

    @PostConstruct
    private void initialise() {
        int CONNECT_TIMEOUT = 5000;
        int READ_TIMEOUT = 20000;
        int POOL_SIZE = 50;
        ConnectionPool pool = new ConnectionPool(POOL_SIZE, 5, TimeUnit.MINUTES);
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(chain -> {
                    Request request = chain
                            .request()
                            .newBuilder()
                            .addHeader("Content-Type", "application/json")
                            .build();
                    return chain.proceed(request);
                })
                .connectionPool(pool)
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(heimdallUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient)
                .build();

        this.callProviderAPI = retrofit.create(CallProviderAPI.class);
    }

    @Override
    public DeliveryExecutiveNumberResponse updateDirectCallStatus(ProviderCallBackPojo providerCallBackPojo) {
        Call<DeliveryExecutiveNumberResponse> executiveNumberResponseCall = callProviderAPI.updateDirectCallStatus(providerCallBackPojo);
        try {
            Response<DeliveryExecutiveNumberResponse> execute = executiveNumberResponseCall.execute();
            if (!execute.isSuccessful()) {
                log.error("error {} in updating direct call back status for firstLegNumber {} and call uuid {}", execute.message(),
                        providerCallBackPojo.getFirstLegNumber(), providerCallBackPojo.getCallUUId());
                return null;
            }
            log.info("successfully updated direct call back status for firstLegNumber {} and call uuid {}", providerCallBackPojo.getFirstLegNumber(),
                    providerCallBackPojo.getCallUUId());
            return execute.body();
        } catch (IOException e) {
            log.error("exception", e);
        }
        return null;
    }

    @Override
    public DeliveryExecutiveNumberResponse getSecondLegNumberResponse(String firstLegNumebr, String virtualNumber, String callUUId) {
        Call<DeliveryExecutiveNumberResponse> leg2NumberResponse = callProviderAPI.getLeg2NumberResponse(firstLegNumebr, virtualNumber, callUUId);
        try {
            Response<DeliveryExecutiveNumberResponse> execute = leg2NumberResponse.execute();
            if (!execute.isSuccessful()) {
                log.error("error {} in fetching de number for firstLegNumber {} and call uuid {}", execute.message(), firstLegNumebr, callUUId);
                return null;
            }
            if (execute.body() != null && execute.body().getSecondLegNumber() != null) {
                log.info("successfully fetched de number for firstLegNumber {} and call uuid {}", firstLegNumebr, callUUId);
            } else {
                log.warn("de number not found for firstLegNumber {} and call uuid {}", firstLegNumebr, callUUId);
            }

            return execute.body();
        } catch (IOException e) {
            log.error("exception", e);
        }

        return null;
    }
}
