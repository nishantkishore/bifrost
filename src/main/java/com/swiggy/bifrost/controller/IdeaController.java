package com.swiggy.bifrost.controller;

import com.swiggy.bifrost.pojo.callprovider.DeliveryExecutiveNumberResponse;
import com.swiggy.bifrost.pojo.callprovider.ProviderCallBackPojo;
import com.swiggy.bifrost.service.CallProviderService;
import com.swiggy.bifrost.validator.IdeaRequestValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/api/idea")
@Slf4j
public class IdeaController {

    private CallProviderService callProviderService;
    private IdeaRequestValidator requestValidator;

    @Autowired
    public IdeaController(CallProviderService callProviderService, IdeaRequestValidator requestValidator) {
        this.callProviderService = callProviderService;
        this.requestValidator = requestValidator;
    }

    @RequestMapping(value = "/direct_call/status", method = POST, produces = APPLICATION_JSON_VALUE)
    public DeliveryExecutiveNumberResponse directCallStatus(@RequestBody ProviderCallBackPojo providerCallBackPojo) {
        log.info("Idea direct call back request: {}", providerCallBackPojo);
        DeliveryExecutiveNumberResponse deliveryExecutiveNumberResponse = requestValidator.validateCallStatusUpdateRequest(providerCallBackPojo);
        if (deliveryExecutiveNumberResponse != null) {
            return deliveryExecutiveNumberResponse;
        }
        return callProviderService.updateDirectCallStatus(providerCallBackPojo);
    }

    @RequestMapping(value = "/direct_call/leg2_number", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public DeliveryExecutiveNumberResponse getLeg2Number(
            @RequestParam(value = "leg1_number") String firstLegNumebr,
            @RequestParam(value = "virtual_number") String virtualNumber,
            @RequestParam(value = "call_uuid") String callUUId,
            @RequestParam(value = "api_key") String apiKey
    ) {
        log.info("idea api request for fetching DE number: cust no: {}, virtual number: {},callUUID : {} , api key: {}",
                firstLegNumebr, virtualNumber, callUUId, apiKey);

        DeliveryExecutiveNumberResponse deliveryExecutiveNumberResponse = requestValidator.validateGetLeg2NumberRequest(firstLegNumebr, virtualNumber, apiKey);
        if (deliveryExecutiveNumberResponse != null) {
            return deliveryExecutiveNumberResponse;
        }

        return callProviderService.getSecondLegNumberResponse(firstLegNumebr,
                virtualNumber, callUUId);

    }
}
