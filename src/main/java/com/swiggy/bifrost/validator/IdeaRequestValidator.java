package com.swiggy.bifrost.validator;

import com.swiggy.bifrost.pojo.callprovider.DeliveryExecutiveNumberResponse;
import com.swiggy.bifrost.pojo.callprovider.ProviderCallBackPojo;
import com.swiggy.bifrost.utils.MD5Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import static org.springframework.util.StringUtils.isEmpty;

@Component
@Slf4j
public class IdeaRequestValidator {

    @Value("${ideaDirectCallSecretKey}")
    private String ideaDirectCallSecretKey;

    public DeliveryExecutiveNumberResponse validateCallStatusUpdateRequest(ProviderCallBackPojo providerCallBackPojo) {
        if (providerCallBackPojo == null || isEmpty(providerCallBackPojo.getVirtualNumber()) ||
                isEmpty(providerCallBackPojo.getFirstLegNumber())) {
            log.error("returning failure response as required fields are not present");
            return DeliveryExecutiveNumberResponse.builder()
                    .status("failure")
                    .error("Required fields not present")
                    .build();
        }

        return validateAPIKey(providerCallBackPojo.getFirstLegNumber(), providerCallBackPojo.getVirtualNumber(), providerCallBackPojo.getApiKey());
    }

    public DeliveryExecutiveNumberResponse validateGetLeg2NumberRequest(String firstLegNumebr, String virtualNumber, String apiKey) {

        if (isEmpty(firstLegNumebr) || isEmpty(virtualNumber)) {
            log.error("returning failure response as either customer number or virtual number is empty");
            return DeliveryExecutiveNumberResponse.builder()
                    .status("failure")
                    .statusCode(3)
                    .error("Empty customer number or virtual number")
                    .build();
        }
        return validateAPIKey(firstLegNumebr, virtualNumber, apiKey);
    }


    private DeliveryExecutiveNumberResponse validateAPIKey(String firstLegNumber, String virtualNumber, String apiKey) {
        String validApiKey = MD5Utils.getMD5(firstLegNumber + ideaDirectCallSecretKey +
                virtualNumber);
        if (validApiKey == null || !validApiKey.equals(apiKey)) {
            log.error("unauthorized to call the application for customer number :{}", firstLegNumber);
            return DeliveryExecutiveNumberResponse.builder()
                    .status("failure")
                    .error("Unauthorized !! Please provide valid authorization key")
                    .statusCode(4)
                    .build();
        }
        return null;
    }
}
