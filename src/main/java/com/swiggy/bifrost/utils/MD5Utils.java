package com.swiggy.bifrost.utils;
import lombok.extern.slf4j.Slf4j;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Slf4j
public class MD5Utils {

	private final static String encodingAlgo = "MD5";

	public static String getMD5(String input) {
		try {
			MessageDigest md = MessageDigest.getInstance(encodingAlgo);
			byte[] messageDigest = md.digest(input.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			String hashtext = number.toString(16);
			while (hashtext.length() < 32) {
				hashtext = "0" + hashtext;
			}
			return hashtext;
		}
		catch (NoSuchAlgorithmException e) {
			log.error("Error in getting MD5 encoding");
		}
		return null;
	}
}