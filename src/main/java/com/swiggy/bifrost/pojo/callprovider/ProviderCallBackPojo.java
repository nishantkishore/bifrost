package com.swiggy.bifrost.pojo.callprovider;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProviderCallBackPojo {

    @JsonProperty("call_time")
    private String callTime;

    @JsonProperty("call_duration_in_seconds")
    private Integer callDuration;

    @JsonProperty("leg2_number")
    private String secondLegNumber;

    @JsonProperty("leg1_number")
    private String firstLegNumber;

    @JsonProperty("call_status")
    private String callStatus;

    @JsonProperty("recording_url")
    private String recordingUrl;

    @JsonProperty("call_uuid")
    private String callUUId;

    @JsonProperty("hangup_cause")
    private String hangupCause;

    @JsonProperty("api_key")
    private String apiKey;

    @JsonProperty("virtual_number")
    private String virtualNumber;

    @JsonProperty("leg1_call_received_time")
    private String firstLegCallReceivedTime;

    @JsonProperty("leg2_call_connected_time")
    private String secondLegCallConnectedTime;

    @JsonProperty("call_end_time")
    private String callEndTime;

}
