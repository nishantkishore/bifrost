package com.swiggy.bifrost.configuration;

import com.swiggy.bifrost.service.CallProviderService;
import com.swiggy.bifrost.service.impl.CallProviderServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan(basePackages = "com.swiggy.bifrost")
@Import(SwaggerConfig.class)
public class CallProviderConfiguration {

    @Bean
    public CallProviderService callProviderService() {
     return new CallProviderServiceImpl();
    }
}
