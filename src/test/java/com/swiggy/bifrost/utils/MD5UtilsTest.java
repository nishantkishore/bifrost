package com.swiggy.bifrost.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class MD5UtilsTest {

    @Test
    public void shouldGeneretaMD5Successfully() {
        assertEquals("a32dd49753b93297f389c3e11f97ae82", MD5Utils.getMD5("9013012802" + "12345678"));
    }
}