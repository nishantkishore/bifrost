package com.swiggy.bifrost.validator;

import com.swiggy.bifrost.pojo.callprovider.DeliveryExecutiveNumberResponse;
import com.swiggy.bifrost.pojo.callprovider.ProviderCallBackPojo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


@RunWith(MockitoJUnitRunner.class)
public class IdeaRequestValidatorTest {

    private IdeaRequestValidator requestValidator;
    @Before
    public void setUp() throws Exception {
        requestValidator = new IdeaRequestValidator();
        ReflectionTestUtils.setField(requestValidator, "ideaDirectCallSecretKey", "slkna9KASJByKbjksa", String.class);
    }

    @Test
    public void shouldReturnFailureResponseForInvalidCallbackStatusUpdateRequest() {
        ProviderCallBackPojo callBackPojo = ProviderCallBackPojo.builder()
                .virtualNumber("12345678").apiKey("0b53d67c468e4d392d605efb0dfe7cec").build();
        DeliveryExecutiveNumberResponse deliveryExecutiveNumberResponse = requestValidator.validateCallStatusUpdateRequest(callBackPojo);

        assertEquals("failure", deliveryExecutiveNumberResponse.getStatus());

    }

    @Test
    public void shouldReturnFailureResponseForInvalidFetchLeg2NumberRequest() {
        ProviderCallBackPojo callBackPojo = ProviderCallBackPojo.builder()
                .virtualNumber("12345678").apiKey("0b53d67c468e4d392d605efb0dfe7cec").build();
        DeliveryExecutiveNumberResponse deliveryExecutiveNumberResponse = requestValidator.validateCallStatusUpdateRequest(callBackPojo);

        assertEquals("failure", deliveryExecutiveNumberResponse.getStatus());
    }

    @Test
    public void shouldValidateCallbackStatusUpdateRequestSuccessfully() {
        ProviderCallBackPojo callBackPojo = ProviderCallBackPojo.builder()
                .firstLegNumber("9013012802").virtualNumber("12345678").apiKey("0b53d67c468e4d392d605efb0dfe7cec").build();
        DeliveryExecutiveNumberResponse deliveryExecutiveNumberResponse = requestValidator.validateCallStatusUpdateRequest(callBackPojo);
        System.setProperty("ideaDirectCallSecretKey", "slkna9KASJByKbjksa");

        assertNull(deliveryExecutiveNumberResponse);
    }
}