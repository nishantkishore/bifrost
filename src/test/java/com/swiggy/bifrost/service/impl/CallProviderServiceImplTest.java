package com.swiggy.bifrost.service.impl;

import com.swiggy.bifrost.pojo.callprovider.DeliveryExecutiveNumberResponse;
import com.swiggy.bifrost.pojo.callprovider.ProviderCallBackPojo;
import com.swiggy.bifrost.service.CallProviderAPI;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CallProviderServiceImplTest extends TestCase {

    @InjectMocks
    private CallProviderServiceImpl callProviderService;
    @Mock
    private CallProviderAPI callProviderAPI;
    private ProviderCallBackPojo providerCallBackPojo;
    @Mock
    private Call<DeliveryExecutiveNumberResponse> deliveryExecutiveNumberResponseCall;

    @Before
    public void setUp() throws Exception {
        providerCallBackPojo = ProviderCallBackPojo.builder().build();
        DeliveryExecutiveNumberResponse deliveryExecutiveNumberResponse = DeliveryExecutiveNumberResponse.builder().build();
        Response<DeliveryExecutiveNumberResponse> response = Response.success(deliveryExecutiveNumberResponse);
        when(callProviderAPI.updateDirectCallStatus(providerCallBackPojo)).thenReturn(deliveryExecutiveNumberResponseCall);
        when(deliveryExecutiveNumberResponseCall.execute()).thenReturn(response);
    }

    @Test
    public void shouldUpdateDirectCallBackStatus() throws IOException {
        DeliveryExecutiveNumberResponse deliveryExecutiveNumberResponse = callProviderService.updateDirectCallStatus(providerCallBackPojo);

        assertNotNull(deliveryExecutiveNumberResponse);
    }
}